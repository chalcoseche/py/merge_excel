# import pandas as pd
# import openpyxl

# 
# membersData =pd.read_excel('./files/members.xlsx')
# 
# print(membersData)



# # 파이썬으로 액셀 파일 생성 / 가공
# import pandas as pd
#
# memberData = pd.read_excel('./files/members.xlsx')
# # historyData = pd.read_excel('./files/history.xlsx')
#
# newFile = openpyxl.Workbook()  # 빈 액셀파일 임시 생성
# # newFileName = './resultFile/result.xlsx' #빈 액셀파일 저장 경로
# # newFile.save(newFileName) #저장명
#
# sheet = newFile.active
# # 액셀 시트 가공
# sheet['B2'].value= "hello"


import pandas as pd
import openpyxl


membersData =pd.read_excel('./files/member.xlsx') # 사원정보
historyData =pd.read_excel('./files/history.xlsx') # 일당정보

membersDataCompanyNumberLines = membersData['사번'] # 사원정보 - 사번 값들 가져오기
membersDataNameLines = membersData['이름']
membersDataJuminLines = membersData['주민번호']

historyDataCompanyNumberLines = historyData['사번'] #히스토리파일 사번값 가져오기
historyDataPayLines = historyData['일당']

priceList =[]   # 급여액을 저장할 리스트 생성

for companyNumber in membersDataCompanyNumberLines: # 사원정보의 사번을 하나 씩 순서대로 던져주기
  tempTotalPrice = 0
  histroyIndex = 0
  for historyCompanyNumber in historyDataCompanyNumberLines:
    if historyCompanyNumber == companyNumber:
      tempTotalPrice += historyDataCompanyNumberLines[historyIndex]
          histroyIndex = histroyIndex + 1 #순번 증가


  priceList.append(tempTotalPrice)

resultDataFrame = pd.DataFrame({
  '사번':membersDataCompanyNumberLines,
  '이름':membersDataNameLines,
  '주민번호':membersDataJuminLines,
  '총급여': priceList

})

newFileName = "./resultFiles/result2.xlsx"
resultDataFrame.to_excel(newFileName,index=False)



# 사원 정보를 하나씩 던져주면셔(for)
# 일당정보 리스트를 한줄씩 읽으면서 사번이 같으면
# 그 사원의 총 급여액에 그 줄의 급여금액을 만든다.
# 해당 사원의 총 급여액을 다 더한게 끝나면 (일당 정보 리스트를 다 읽은 후)
# priceList에 계산된 총 급여액을 추가한다.(그러면 사원정보 index와 priceList index번호가 같아짐)
# 데이터 가공후 액셀 파일 만들때 인덱스 번호 맞춰서 금액을 맞춰 작성한다.
for item in membersData:
  print(membersData[item])